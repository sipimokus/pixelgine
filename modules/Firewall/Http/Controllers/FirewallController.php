<?php namespace Modules\Firewall\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class FirewallController extends Controller {
	
	public function index()
	{
		return view('firewall::index');
	}
	
}