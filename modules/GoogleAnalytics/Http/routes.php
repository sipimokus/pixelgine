<?php

Route::group(['prefix' => 'googleanalytics', 'namespace' => 'Modules\GoogleAnalytics\Http\Controllers'], function()
{
	Route::get('/', 'GoogleAnalyticsController@index');
});