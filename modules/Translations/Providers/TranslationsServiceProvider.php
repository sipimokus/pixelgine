<?php namespace Modules\Translations\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class TranslationsServiceProvider extends ServiceProvider {

	/**
	 * The class aliases array.
	 *
	 * @var array
	 */
	protected $aliases = [];

	/**
	 * The service provider classes array.
	 *
	 * @var array
	 */
	protected $providers = [
		'Barryvdh\TranslationManager\ManagerServiceProvider',
		'Barryvdh\TranslationManager\TranslationServiceProvider',
		'Overtrue\LaravelLang\TranslationServiceProvider'
	];

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		$this->registerConfig();
		$this->registerTranslations();
		$this->registerViews();
		$this->registerMenus();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerProviders();
		$this->registerAliases();

		$this->app->bind(
			'Barryvdh\TranslationManager\TranslationServiceProvider',
			'Illuminate\Translation\TranslationServiceProvider'
		);
	}

	/**
	 * Register service providers.
	 */
	public function registerProviders()
	{
		foreach ($this->providers as $provider) {
			$this->app->register($provider);
		}
	}

	/**
	 * Register class aliases.
	 */
	public function registerAliases()
	{
		AliasLoader::getInstance($this->aliases)->register();
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('translations.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'translations'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/translations');

		$sourcePath = __DIR__.'/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath
		]);

		$this->loadViewsFrom([$viewPath, $sourcePath], 'translations');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/translations');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'translations');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'translations');
		}
	}

	/**
	 * Register menus
	 *
	 * @return void
	 */
	public function registerMenus()
	{
		$menu = realpath(__DIR__ . '/../menus.php');

		if ( file_exists( $menu ) )
			include( $menu );
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
