<?php namespace Modules\Forms\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class FormsController extends Controller {
	
	public function index()
	{
		return view('forms::index');
	}
	
}