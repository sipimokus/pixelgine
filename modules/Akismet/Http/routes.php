<?php

Route::group(['prefix' => 'akismet', 'namespace' => 'Modules\Akismet\Http\Controllers'], function()
{
	Route::get('/', 'AkismetController@index');
});