<?php namespace Modules\Mediamanager\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class MediaManagerController extends Controller {
	
	public function index()
	{
		return view('mediamanager::index');
	}
	
}