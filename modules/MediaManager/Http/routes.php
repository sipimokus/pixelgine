<?php

Route::group(['prefix' => 'mediamanager', 'namespace' => 'Modules\MediaManager\Http\Controllers'], function()
{
	Route::get('/', 'MediaManagerController@index');
});