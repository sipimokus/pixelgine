<?php

Route::group(['prefix' => 'dynamicdata', 'namespace' => 'Modules\DynamicData\Http\Controllers'], function()
{
	Route::get('/', 'DynamicDataController@index');
});