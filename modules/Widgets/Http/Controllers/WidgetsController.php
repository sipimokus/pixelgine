<?php namespace Modules\Widgets\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class WidgetsController extends Controller {
	
	public function index()
	{
		return view('widgets::index');
	}
	
}