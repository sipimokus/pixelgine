<?php namespace Modules\Pushnotification\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class PushnotificationController extends Controller {
	
	public function index()
	{
		return view('pushnotification::index');
	}
	
}