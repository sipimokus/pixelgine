<?php

Route::group(['prefix' => 'datahistory', 'namespace' => 'Modules\DataHistory\Http\Controllers'], function()
{
	Route::get('/', 'DataHistoryController@index');
});