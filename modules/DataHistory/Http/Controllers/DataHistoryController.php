<?php namespace Modules\Datahistory\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class DataHistoryController extends Controller {
	
	public function index()
	{
		return view('datahistory::index');
	}
	
}