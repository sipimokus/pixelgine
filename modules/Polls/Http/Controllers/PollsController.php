<?php namespace Modules\Polls\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class PollsController extends Controller {
	
	public function index()
	{
		return view('polls::index');
	}
	
}