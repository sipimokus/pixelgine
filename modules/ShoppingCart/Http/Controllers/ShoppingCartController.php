<?php namespace Modules\Shoppingcart\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class ShoppingCartController extends Controller {
	
	public function index()
	{
		return view('shoppingcart::index');
	}
	
}