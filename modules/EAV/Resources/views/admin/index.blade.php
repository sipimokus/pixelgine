@extends($layout)

@section('content-header')
	<h1>
		{!! $title or 'All EAV Models' !!} ({!! count($models) !!})
		&middot;
		<small>{!! link_to_route('admin.eav.create', trans('eav::models.create')) !!}</small>
	</h1>
@stop

@section('content')


	@if(isset($search))
		@include('admin::users.search-form')
	@endif

	<table class="table">
		<thead>
		<th width="1"></th>
		<th>{{ trans('modulemanager::modules.table.name') }}</th>
		<th>{{ trans('modulemanager::modules.table.description') }}</th>
		<th width="10%" class="text-center">{{ trans('modulemanager::modules.table.action') }}</th>
		</thead>
		<tbody>
		@foreach ($models as $model)
			<tr>
				<td><i class="fa fa-{!! $model->enabled ? 'check' : 'times' !!}"></i></td>
				<td>{!! $model->model !!}</td>
				<td>{!! $model->description !!}</td>
				<td class="text-center">
				@if ($model->enabled)
					{!! modal_popup(route('admin.eav.disable', $model->id), trans('eav::model.disable'), trans('modulemanager::modules.modal.disable', ['name' => $model->model])) !!}
				@else
					{!! modal_popup(route('admin.eav.enable',  $model->id), trans('eav::model.enable'),  trans('modulemanager::modules.modal.enable',  ['name' => $model->model])) !!}
				@endif
				&middot;
				<a href="{!! route('admin.eav.edit', $model->id) !!}">{{ trans('eav::model.edit') }}</a>
				&middot;
				<a href="{!! route('admin.eav.data', $model->id) !!}">{{ trans('eav::model.data') }}</a>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>

	<div class="text-center">

	</div>

	@foreach( $models AS $model )
		{{ $model->model }}:<br>
		@foreach( $model->entities AS $entity )
			@foreach( $entity->values AS $data )
				{{ $data->value }} - {{ $data->field->attribute }}<br>
			@endforeach
		@endforeach
		<hr>
	@endforeach
	<hr>

@stop
