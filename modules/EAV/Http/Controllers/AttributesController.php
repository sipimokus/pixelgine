<?php namespace Modules\Eav\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Modules\Eav\Forms\AttributeEditForm;
use Modules\Eav\Models\Attribute;
use Pingpong\Modules\Routing\Controller;
use Kris\LaravelFormBuilder\FormBuilder;


class AttributesController extends Controller
{
	protected $enums = [];
	protected $enumNames = [];
	protected $form  = null;


	public function edit( FormBuilder $formBuilder, $id )
	{
		$this->form = $formBuilder->create(AttributeEditForm::class, [
			'method' => 'POST',
			'url'  	 => route('admin.eav.attribute.store', $id)
		])->modify('type', 'select', [
			'choices' => $this->enums,
			'rules'   => 'required|in:' . implode(',', $this->enumNames),
		]);

		$model = Model::find( $id );

		$formAttribute = $this->form;
		$enums = $this->enums;

		return view('eav::admin/edit', compact('model','formAttribute','enums'));
	}

	/*public function enable( $id )
	{
		$model = Model::find( $id );
		$model->enabled = 1;

		if ( !$model->save() )
		{
			return Redirect::route('admin.eav.index')
				->withFlashMessage( $model->model. ' model not enabled!' )
				->withFlashType('danger');
		}

		return Redirect::route('admin.eav.index')
			->withFlashMessage( $model->model. ' model enabled!' )
			->withFlashType('success');
	}

	public function disable( $id )
	{
		$model = Model::find( $id );
		$model->enabled = 0;

		if ( !$model->save() )
		{
			return Redirect::route('admin.eav.index')
				->withFlashMessage( $model->model. ' model not disabled!' )
				->withFlashType('danger');
		}

		return Redirect::route('admin.eav.index')
			->withFlashMessage( $model->model. ' model disabled!' )
			->withFlashType('success');
	}*/

	public function store( FormBuilder $formBuilder )
	{
		$this->form = $formBuilder->create(AttributeEditForm::class)->modify('type', 'select', [
			'choices' => $this->enums,
			'rules' => 'required|in:' . implode(',', Attribute::enumNames()),
		]);

		// It will automatically use current request, get the rules, and do the validation
		if ( !$this->form->isValid() )
		{
			return redirect()->back()->withErrors(
				$this->form->getErrors()
			)->withInput();
		}

		$input = Input::all();

		//Attribute::unguard();
		$model = Attribute::create( $input );
		//Attribute::reguard();

		if ( !$model )
		{
			return Redirect::back()
				->withFlashMessage( $input['attribute']. ' attribute not created!' )
				->withFlashType('danger');
		}

		return Redirect::back()
			->withFlashMessage( $input['attribute']. ' attribute created!' )
			->withFlashType('success');
	}

	public function destroy( $id )
	{
		$model = Attribute::findOrFail( $id );
		$name  = $model->attribute;
		if ( !Attribute::destroy( $id ) )
		{
			return Redirect::back()
				->withFlashMessage( $name. ' attribute not deleted!' )
				->withFlashType('danger');
		}

		return Redirect::back()
			->withFlashMessage( $name. ' attribute deleted!' )
			->withFlashType('success');
	}
	
}