<?php namespace Modules\Eav\Http\Controllers;

//use App\Http\Requests\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Modules\Eav\Models\Attribute;
use Modules\Eav\Models\Model;
use Pingpong\Modules\Routing\Controller;
use Kris\LaravelFormBuilder\FormBuilder;

use Modules\Eav\Forms\AttributeEditForm;


class EAVController extends Controller
{
	protected $form  = null;

	public function index()
	{
		$models = Model::all();

		return view('eav::admin/index', compact('models'));
	}

	public function edit( FormBuilder $formBuilder, $id )
	{
		$this->form = $formBuilder->create(AttributeEditForm::class, [
			'method' => 'POST',
			'url'  	 => route('admin.eav.attribute.store')
		])->modify('type', 'select', [
			'choices' => Attribute::enumList(),
			'rules'   => 'required|in:' . implode(',', Attribute::enumNames()),
		])->modify('model_id', 'hidden', [
			'value' => $id
		]);

		$model = Model::find( $id );

		$formAttribute = $this->form;
		$enums = Attribute::enumList();

		return view('eav::admin/edit', compact('model','formAttribute','enums'));
	}

	public function enable( $id )
	{
		$model = Model::find( $id );
		$model->enabled = 1;

		if ( !$model->save() )
		{
			return Redirect::route('admin.eav.index')
				->withFlashMessage( $model->model. ' model not enabled!' )
				->withFlashType('danger');
		}

		return Redirect::route('admin.eav.index')
			->withFlashMessage( $model->model. ' model enabled!' )
			->withFlashType('success');
	}

	public function disable( $id )
	{
		$model = Model::find( $id );
		$model->enabled = 0;

		if ( !$model->save() )
		{
			return Redirect::route('admin.eav.index')
				->withFlashMessage( $model->model. ' model not disabled!' )
				->withFlashType('danger');
		}

		return Redirect::route('admin.eav.index')
			->withFlashMessage( $model->model. ' model disabled!' )
			->withFlashType('success');
	}
	
}