<?php


Route::group(['prefix' => config('admin.prefix', 'admin'), 'namespace' => 'Modules\EAV\Http\Controllers'], function()
{
	Route::group(['middleware' => config('admin.filter.auth')], function ()
	{
		Route::resource('eav', 'EAVController', [
			'except' => 'show',
			'names' => [
				'index' 	=> 'admin.eav.index',
				'create' 	=> 'admin.eav.create',
				'store' 	=> 'admin.eav.store',
				'show' 		=> 'admin.eav.show',
				'update' 	=> 'admin.eav.update',
				'edit' 		=> 'admin.eav.edit',
				'destroy' 	=> 'admin.eav.destroy',
			],
		]);
		Route::get('eav/{id}/enable',  ['as' => 'admin.eav.enable',  'uses' => 'EAVController@enable'] );
		Route::get('eav/{id}/disable', ['as' => 'admin.eav.disable', 'uses' => 'EAVController@disable']);
		Route::get('eav/{id}/data',    ['as' => 'admin.eav.data',  	 'uses' => 'EAVController@data'] );
		Route::post('eav/attribute', [
			'as' => 'admin.eav.attribute.store',  'uses' => 'AttributesController@store'
		] );
		Route::get('eav/attribute/{id}/destroy',  [
			'as' => 'admin.eav.attribute.destroy',  'uses' => 'AttributesController@destroy'
		] );
	});
});
