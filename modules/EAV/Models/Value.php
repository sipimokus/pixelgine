<?php namespace Modules\Eav\Models;

class Value extends EavBaseModel
{
    protected $table = 'eav_data';

    public function field()
    {
        return $this->hasOne('\Modules\Eav\Models\Attribute', 'id', 'attribute_id');
    }

    /*public function filed()
    {
        return $this->belongsTo('\Modules\Eav\Models\Attribute', 'custom_field_id', 'id');
    }

    public function entity()
    {
        return $this->morphTo();
    }*/

    /*public function entity()
    {
        return $this->hasOne('\Modules\Eav\Models\Object');
    }

    public function attribute()
    {
        return $this->hasOne('\Modules\Eav\Models\Attribute');
    }*/

    /*
    public function attribute()
    {
        return $this->morphTo();
    }*/

    /*public function attribute()
    {
        return $this->hasOne('\Modules\Eav\Models\Attribute');
    }

    public function attributes()
    {
        return $this->hasMany('\Modules\Eav\Models\Attribute');
    }*/

    /*public function values(){
        //return ;
    }*/
}