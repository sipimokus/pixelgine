<?php namespace Modules\Eav\Models;

class Model extends EavBaseModel
{
    protected $table = 'eav_model';

    /**
     * Modellhez kapcsolódó entitások
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entities()
    {
        //return $this->belongsToMany('\Modules\Eav\Models\Entity', 'eav_data');
        return $this->hasMany('\Modules\Eav\Models\Entity', 'model_id', 'id');
    }

    public function attributes()
    {
        return $this->hasMany('\Modules\Eav\Models\Attribute', 'model_id', 'id');
    }

/*
    public function entity()
    {

    }
*/

/*
    public function values()
    {
        return $this->hasMany('\Modules\Eav\Models\Value', 'custom_field_id', 'id');
    }
*/
    /*public function values()
    {
        return $this->hasOne('\Modules\Eav\Models\Value');
        //return $this->belongsTo('\Modules\Eav\Models\Value');
        //return $this->belongsTo('\Modules\Eav\Models\Value', 'eav_data', 'value_id');

    }*/

    /*
    public function entity()
    {
        return $this->hasOne('\Modules\Eav\Models\Object');
    }

    public function values()
    {
        return $this->hasMany('\Modules\Eav\Models\Value');
    }*/

    /*public function attributes()
    {
        return $this->hasMany('\Modules\Eav\Models\Value');
        //return $this->hasMany('Record');
    }*/

    /*public function photos()
    {
        return $this->morphMany('\Modules\Eav\Models\Value', 'imageable');;
    }*/

}