<?php namespace Modules\Eav\Models;

class Entity extends EavBaseModel
{
    protected $table  = 'eav_entity';

    /**
     * Entitáshoz tartozó attributumok
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    /*public function fields()
    {
        //return $this->hasMany('\Modules\Eav\Models\Attribute', 'entity_id', 'id');
        return $this->belongsTo('\Modules\Eav\Models\Attribute');
        //return $this->hasMany('\Modules\Eav\Models\Attribute');
    }*/

    public function values()
    {
        return $this->hasMany('\Modules\Eav\Models\Value');
    }


    //protected $entity = null;



    /*public function __construct( $entityName = null )
    {
        $this->entity = $entityName;

        parent::__construct();
    }*/

    /*public function values()
    {
        return $this->hasMany('\Modules\Eav\Models\Value');
        //return $this->morphMany('\Modules\Eav\Models\Value', 'entity');
    }*/


    /*public function value()
    {
        return $this->hasMany('\Modules\Eav\Models\Value');
        //return $this->belongsToMany('\Modules\Eav\Models\Value', 'eav_data', 'entity_id');
    }*/

    /*public function attributes()
    {
        //return $this->hasMany('\Modules\Eav\Models\Attribute');
        return $this->belongsToMany('\Modules\Eav\Models\Attribute', 'eav_data', 'entity_id');
    }*/



    /*public function attribute()
    {
        return $this->morphMany('\Modules\Eav\Models\Value', 'attribute');
    }*/

    /*public function objects()
    {
        return $this->hasMany('\Modules\Eav\Models\Attribute', 'entity_id');
    }*/
/*
    public function attributes()
    {
        return $this->hasMany('\Modules\Eav\Models\Attribute', 'entity_id');
    }

    public function values()
    {
        return $this->hasMany('\Modules\Eav\Models\Value', 'entity_id');
    }*/

    /**
     * Overrides (Laravel or)Illuminate\Database\Eloquent\Model 's query()
     *
     * @param bool $excludeDeleted
     * @return \Illuminate\Database\Eloquent\Builder
     */
    /*public function newQuery( $excludeDeleted = true )
    {
        $query = parent::newQuery( $excludeDeleted );
        // A much better way to restrict access to all banned (status = 0) users
        // from beginning (including search, profile views etc.), disabling them
        // to access anything even if they were logged in previously.
        //$this->entity = 'Contacts';
        if ( $this->entity )
            $query->where('entity', '=', $this->entity);

        return $query;
    }*/
}