<?php namespace Modules\Eav\Models;

class Attribute extends EavBaseModel
{
    protected $table = 'eav_attribute';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['model_id', 'attribute', 'type', 'validation'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected static $enums = [];
    protected static $enumNames = [];

    public function scopeEnumList()
    {
        if ( !empty( self::$enums ) )
            return self::$enums;

        // Setting attribute type enumeration list
        self::$enums[ trans('eav::model.enums.group.textandnumber') ] = [
            'CHAR'		=> 'Karakter',
            'FLOAT'		=> 'Tört szám',
            'INTEGER'	=> 'Egész szám',
            'STRING'	=> 'Egysoros szöveg',
            'TEXT'		=> 'Többsoros szöveg',
            'WYSIWYG'	=> 'Formázott szöveg',
        ];
        self::$enums[ trans('eav::model.enums.group.form') ] = [
            'BOOLEAN'	=> 'Jelölő négyzet',
            'EMAIL'		=> 'E-mail',
        ];
        self::$enums[ trans('eav::model.enums.group.dateandtime') ] = [
            'DATE'		=> 'Dátum',
            'DATETIME'	=> 'Dátum és idő',
            'TIME'		=> 'Idő',
            'TIMEZONE'	=> 'Időzóna',
        ];
        self::$enums[ trans('eav::model.enums.group.storage') ] = [
            'FILE'		=> 'Fájl',
            'IMAGE'		=> 'Kép',
        ];

        return self::$enums;

        /*
    Képhivatkozás
    Csatolt adat szett
*/

    }

    public function scopeEnumNames()
    {
        $this->scopeEnumList();

        if ( empty( self::$enumNames ) )
        foreach( self::$enums AS $keyOff => $enumList )
            foreach( $enumList AS $enumName => $enumText )
                self::$enumNames[] = $enumName;

        return self::$enumNames;
    }

    /*public function values()
    {
        return $this->hasMany('\Modules\Eav\Models\Value', 'custom_field_id', 'id');
    }*/

    /*public function values()
    {
        return $this->hasOne('\Modules\Eav\Models\Value');
        //return $this->belongsTo('\Modules\Eav\Models\Value');
        //return $this->belongsTo('\Modules\Eav\Models\Value', 'eav_data', 'value_id');

    }*/

    /*
    public function entity()
    {
        return $this->hasOne('\Modules\Eav\Models\Object');
    }

    public function values()
    {
        return $this->hasMany('\Modules\Eav\Models\Value');
    }*/

    /*public function attributes()
    {
        return $this->hasMany('\Modules\Eav\Models\Value');
        //return $this->hasMany('Record');
    }*/

    /*public function photos()
    {
        return $this->morphMany('\Modules\Eav\Models\Value', 'imageable');;
    }*/

}