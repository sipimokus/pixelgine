<?php

    $leftMenu  = Menu::instance('admin-menu');
    $rightMenu = Menu::instance('admin-menu-right');

    /**
     * @see https://github.com/pingpong-labs/menus
     *
     * @example adding additional menu.
     *
     * $leftMenu->url('your-url', 'The Title');
     *
     * $leftMenu->route('your-route', 'The Title');
     */
    $leftMenu->enableOrdering();
    $leftMenu->route('admin.eav.index', trans('eav::models.title'), [], 100, ['icon' => 'fa fa-puzzle-piece']);
/*
    $leftMenu->dropdown(trans('modulemanager::modules.title'), function ($sub) {
        $sub->route('admin.modulemanager.index',  trans('modulemanager::modules.all'),    [], 1);
        $sub->route('admin.modulemanager.create', trans('modulemanager::modules.create'), [], 2);
    }, 100, ['icon' => 'fa fa-puzzle-piece']);
*/
