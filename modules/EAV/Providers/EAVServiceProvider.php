<?php namespace Modules\Eav\Providers;

use Illuminate\Support\ServiceProvider;

class EAVServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		require __DIR__ . '/../Http/routes.php';
		require __DIR__ . '/../composers.php';

		$this->registerConfig();
		$this->registerTranslations();
		$this->registerViews();
		$this->registerMenus();
		$this->registerHelpers();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{		
		//
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('eav.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'eav'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/eav');

		$sourcePath = __DIR__.'/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath
		]);

		$this->loadViewsFrom([$viewPath, $sourcePath], 'eav');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/eav');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'eav');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'eav');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

	/**
	 * Register menus
	 *
	 * @return void
	 */
	public function registerMenus()
	{
		$menu = realpath(__DIR__ . '/../menus.php');

		if ( file_exists( $menu ) )
			include( $menu );
	}

	/**
	 * Register the helpers file.
	 */
	public function registerHelpers()
	{
		require __DIR__ . '/../helpers.php';
	}
}
