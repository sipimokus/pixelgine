<?php namespace Modules\Eav\Forms;

use Kris\LaravelFormBuilder\Form as Form;

class AttributeEditForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('attribute', 'text', [
                'rules' => 'required|min:3',
                'label' => false
            ])
            ->add('type', 'select', [
                'empty_value' => trans('eav::model.table.attribute.type_select'),
                'label' => false,
            ])
            ->add('validation', 'text', [
                'rules' => '',
                'label' => false
            ])
            ->add('model_id', 'hidden', [
                'rules' => 'required|integer',
                'label' => false
            ])
            ->add('save', 'submit', [
                'label' => 'Add'
            ]);
    }
}