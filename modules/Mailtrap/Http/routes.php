<?php

Route::group(['prefix' => 'mailtrap', 'namespace' => 'Modules\Mailtrap\Http\Controllers'], function()
{
	Route::get('/', 'MailtrapController@index');
});