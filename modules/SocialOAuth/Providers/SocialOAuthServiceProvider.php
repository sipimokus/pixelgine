<?php namespace Modules\Socialoauth\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class SocialOAuthServiceProvider extends ServiceProvider {

	/**
	 * The class aliases array.
	 *
	 * @var array
	 */
	protected $aliases = [
		'Socialite' => 'Laravel\Socialite\Facades\Socialite',
	];

	/**
	 * The service provider classes array.
	 *
	 * @var array
	 */
	protected $providers = [
		'Laravel\Socialite\SocialiteServiceProvider'
	];

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		$this->registerConfig();
		$this->registerTranslations();
		$this->registerViews();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerProviders();
		$this->registerAliases();
	}

	/**
	 * Register service providers.
	 */
	public function registerProviders()
	{
		foreach ($this->providers as $provider) {
			$this->app->register($provider);
		}
	}

	/**
	 * Register class aliases.
	 */
	public function registerAliases()
	{
		AliasLoader::getInstance($this->aliases)->register();
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('socialoauth.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'socialoauth'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/socialoauth');

		$sourcePath = __DIR__.'/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath
		]);

		$this->loadViewsFrom([$viewPath, $sourcePath], 'socialoauth');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/socialoauth');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'socialoauth');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'socialoauth');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
