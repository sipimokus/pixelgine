<?php namespace Modules\Mailchimp\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class MailChimpController extends Controller {
	
	public function index()
	{
		return view('mailchimp::index');
	}
	
}