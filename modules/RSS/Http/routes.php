<?php

Route::group(['prefix' => 'rss', 'namespace' => 'Modules\RSS\Http\Controllers'], function()
{
	Route::get('/', 'RSSController@index');
});