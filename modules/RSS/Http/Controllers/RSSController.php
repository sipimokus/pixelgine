<?php namespace Modules\Rss\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class RSSController extends Controller {
	
	public function index()
	{
		return view('rss::index');
	}
	
}