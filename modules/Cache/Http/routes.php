<?php

Route::group(['prefix' => 'cache', 'namespace' => 'Modules\Cache\Http\Controllers'], function()
{
	Route::get('/', 'CacheController@index');
});