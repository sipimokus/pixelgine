<?php namespace Modules\Digitalocean\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class DigitalOceanController extends Controller {
	
	public function index()
	{
		return view('digitalocean::index');
	}
	
}