<?php

Route::group(['prefix' => 'digitalocean', 'namespace' => 'Modules\DigitalOcean\Http\Controllers'], function()
{
	Route::get('/', 'DigitalOceanController@index');
});