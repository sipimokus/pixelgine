<?php

Route::group(['prefix' => 'dropbox', 'namespace' => 'Modules\Dropbox\Http\Controllers'], function()
{
	Route::get('/', 'DropboxController@index');
});