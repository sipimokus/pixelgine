<?php

Route::group(['prefix' => 'officeexcel', 'namespace' => 'Modules\OfficeExcel\Http\Controllers'], function()
{
	Route::get('/', 'OfficeExcelController@index');
});