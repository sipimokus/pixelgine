<?php namespace Modules\Officeword\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use \PhpOffice\PhpWord\PhpWord;

class OfficeWordServiceProvider extends ServiceProvider
{

	/**
	 * The class aliases array.
	 *
	 * @var array
	 */
	protected $aliases = [
		'Word' => 'Modules\Officeword\Facades\Word',
	];

	/**
	 * The service provider classes array.
	 *
	 * @var array
	 */
	protected $providers = [];

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		$this->registerConfig();
		$this->registerTranslations();
		$this->registerViews();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$app = $this->app;

		// create image
		$app['word'] = $app->share(function ($app) {

			//$configs = $app['config']->get('word');

			return new PhpWord();

			//if(!empty($configs['key'])) {
			//	return new \Redmine\Client($configs['url'], $configs['key']);
			//} else {
			//	return new \Redmine\Client($configs['url'], $configs['username'], $configs['password']);
			//}

		});

		//$this->registerProviders();
		$this->registerAliases();
	}

	/**
	 * Register service providers.
	 */
	public function registerProviders()
	{
		foreach ($this->providers as $provider) {
			$this->app->register($provider);
		}
	}

	/**
	 * Register class aliases.
	 */
	public function registerAliases()
	{
		AliasLoader::getInstance($this->aliases)->register();
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('officeword.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'officeword'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/officeword');

		$sourcePath = __DIR__.'/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath
		]);

		$this->loadViewsFrom([$viewPath, $sourcePath], 'officeword');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/officeword');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'officeword');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'officeword');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
