<?php

return array (
  'menus' => 
  array (
    'dashboard' => 'Dashboard',
    'articles' => 
    array (
      'title' => 'Articles',
      'all' => 'All Articles',
      'create' => 'Add New',
    ),
    'pages' => 
    array (
      'title' => 'Pages',
      'all' => 'All Pages',
      'create' => 'Add New',
    ),
    'users' => 
    array (
      'title' => 'Users',
      'all' => 'All Users',
      'create' => 'Add New',
    ),
    'roles' => 'Roles',
    'permissions' => 'Permissions',
    'categories' => 'Categories',
  ),
);
