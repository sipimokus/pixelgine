@extends($layout)

@section('content-header')
	<h1>
		{!! $title or 'All Modules' !!} ({!! count($modules) !!})
		&middot;
		<small>{!! link_to_route('admin.modulemanager.create', trans('modulemanager::modules.create')) !!}</small>
	</h1>
@stop

@section('content')

	@if(isset($search))
		@include('admin::users.search-form')
	@endif

	<table class="table">
		<thead>
			<th width="1"></th>
			<th>{{ trans('modulemanager::modules.table.name') }}</th>
			<th>{{ trans('modulemanager::modules.table.description') }}</th>
			<th class="hidden-xs" width="15%">{{ trans('modulemanager::modules.table.tags') }}</th>
			<th width="10%" class="text-center">{{ trans('modulemanager::modules.table.action') }}</th>
		</thead>
		<tbody>
			@foreach ($modules as $module)
				{{-- dd($module)  --}}
			<tr>
				<td><i class="fa fa-{!! $module->enabled() ? 'check' : 'times' !!}"></i></td>
				<td>{!! $module->get('name') !!}</td>
				<td>{!! $module->getDescription() !!}</td>
				<td class="hidden-xs"><span class="badge">{!! implode('</span><span class="badge">', $module->get('keywords')) !!}</span></td>
				<td class="text-center">
				@if ( strtolower($module->getStudlyName()) != strtolower(config('modulemanager.name')) )
					@if ($module->enabled())
						{!! modal_popup(route('admin.modulemanager.disable', $module->get('name')), trans('modulemanager::modules.disable'), trans('modulemanager::modules.modal.disable', ['name' => $module->get('name')]))!!}
					@else
						{!! modal_popup(route('admin.modulemanager.enable',  $module->get('name')), trans('modulemanager::modules.enable'),  trans('modulemanager::modules.modal.enable',  ['name' => $module->get('name')]))!!}
					@endif

					&middot;
					<a href="{!! route('admin.modulemanager.edit', $module->get('name')) !!}">{{ trans('modulemanager::modules.edit') }}</a>
				@endif
				</td>
			</tr>
			<?php /*$no++*/ ;?>
			@endforeach
		</tbody>
	</table>

	<div class="text-center">

	</div>
@stop
