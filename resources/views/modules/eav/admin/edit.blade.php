@extends($layout)

@section('content-header')
	<h1>
		{{ $model->model }} EAV Model
		&middot;
		<small>{!! link_to_route('admin.eav.index', trans('eav::model.back')) !!}</small>
	</h1>
@stop

@section('content')
	{{--
	<div>
		@include('modulemanager::admin.form', array('model' => $module))
	</div>
	--}}

	<div>
		<table class="table">
			<thead>
				<tr>
					<th>{{ trans('eav::model.table.name') }}</th>
					<th>{{ trans('eav::model.table.description') }}</th>
					<th>{{ trans('eav::model.table.alias') }}</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $model->model }}</td>
					<td>{{ $model->description }}</td>
					<td></td>
				</tr>
			</tbody>
		</table>

		{!! form_start($formAttribute) !!}
		<table class="table">
			<thead>
				<tr>
					<th>{{ trans('eav::model.table.attribute.name') }}</th>
					<th>{{ trans('eav::model.table.attribute.type') }}</th>
					<th>{{ trans('eav::model.table.attribute.validation') }}</th>
					<th width="1"></th>
				</tr>
			</thead>
			<tbody>
			@foreach( $model->attributes AS $attribute )
				{{-- dd($attribute) --}}
				<tr>
					<td>{{ $attribute->attribute }}</td>
					<td>{{ array_recursive_find($enums, $attribute->type) }}</td>
					<td>{{ $attribute->validation }}</td>
					<td>{!! modal_popup(route('admin.eav.attribute.destroy', $attribute->id), 'Delete', trans('eav::attribute.modal.delete', ['name' => $attribute->attribute]))!!}</td>
				</tr>
			@endforeach
			</tbody>
			<tbody>
				<tr>
					<td>{!! form_row($formAttribute->attribute) !!}</td>
					<td>{!! form_row($formAttribute->type) !!}</td>
					<td>{!! form_row($formAttribute->validation) !!}</td>
					<td>
						{!! form_row($formAttribute->model_id) !!}
						{!! form_row($formAttribute->save) !!}
					</td>
				</tr>
			</tbody>
		</table>
		{!! form_end($formAttribute, false) !!}


			{!! modal_popup(route('admin.eav.destroy', $model->model), 'Delete module', trans('eav::model.modal.delete', ['name' => $model->model]))!!}
	</div>
@stop
