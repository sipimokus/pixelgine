<?php namespace Pixellab\Modulemanager\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Pingpong\Modules\Routing\Controller;
use Module;

class ModuleManagerController extends Controller
{
	public function index()
	{
		$modules = $this->allOrSearch( Input::get('q') );
		$no = 1;

		return view('modulemanager::admin/index', compact('modules', 'no'));
	}

	public function edit( $name )
	{
		$module = Module::find( $name );

		return view('modulemanager::admin/edit', compact('module'));
	}

	public function enable( $name )
	{
		$module = Module::find( $name );
		$module->enable();

		if ( $module->disabled() )
		{
			return Redirect::route('admin.modulemanager.index')
				->withFlashMessage( $module->getStudlyName(). ' module not enabled!' )
				->withFlashType('danger');
		}

		return Redirect::route('admin.modulemanager.index')
			->withFlashMessage( $module->getStudlyName(). ' module enabled!' )
			->withFlashType('success');
	}

	public function disable( $name )
	{
		$module = Module::find( $name );
		$module->disable();

		if ( $module->enabled() )
		{
			return Redirect::route('admin.modulemanager.index')
				->withFlashMessage( $module->getStudlyName(). ' module not disabled!' )
				->withFlashType('danger');
		}

		return Redirect::route('admin.modulemanager.index')
			->withFlashMessage( $module->getStudlyName(). ' module disabled!' )
			->withFlashType('success');
	}

	public function install( $name )
	{
		$module = Module::install( $name );
		$name = ucwords($name);

		if ( !$module )
		{
			return Redirect::route('admin.modulemanager.edit', $name)
				->withFlashMessage( $name. ' module not installed!' )
				->withFlashType('danger');
		}

		return Redirect::route('admin.modulemanager.edit', $name)
			->withFlashMessage( $name. ' module installed!' )
			->withFlashType('success');
	}

	public function create()
	{

	}

	public function store()
	{
		/*Artisan::call('migrate', [
			'--package'=>'cartalyst/sentry'
		]);*/
		// http://laravel-tricks.com/tricks/run-artisan-commands-form-route-or-controller
	}

	public function destroy( $name )
	{
		// delete module
		$module = Module::find( $name );
		$module->delete();

		// check module
		$module = Module::find( $name );
		$name = ucwords($name);

		if ( $module )
		{
			return Redirect::route('admin.modulemanager.edit')
				->withFlashMessage( $name. ' module not deleted!' )
				->withFlashType('danger');
		}

		return Redirect::route('admin.modulemanager.index')
			->withFlashMessage( $name. ' module deleted!' )
			->withFlashType('success');
	}

	private function allOrSearch( $query = null )
	{
		$modules = Module::all();
		if ( $query )
		{
			$query = strtolower( $query );
			foreach ( $modules as $key => $module)
			{
				if( strpos( strtolower($module->getStudlyName()), $query ) === false
				&&  strpos( strtolower($module->getName()), $query ) 	   === false )
				{
					unset($modules[$key]);
				}

			}
		}

		return $modules;
	}
}