<?php namespace Pixellab\Modulemanager\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Auth;

class ModuleManagerServiceProvider extends ServiceProvider
{
	/**
	 * The class aliases array.
	 *
	 * @var array
	 */
	protected $aliases = [
		'Module' => 'Pingpong\Modules\Facades\Module',
	];

	/**
	 * The service provider classes array.
	 *
	 * @var array
	 */
	protected $providers = [
		'Pingpong\Modules\ModulesServiceProvider',
	];

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		require __DIR__ . '/../Http/routes.php';
		require __DIR__ . '/../composers.php';

		$this->registerConfig();
		$this->registerTranslations();
		$this->registerViews();
		$this->registerMenus();
		$this->registerPermissions();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerProviders();
		$this->registerAliases();
	}

	/**
	 * Register service providers.
	 */
	public function registerProviders()
	{
		foreach ($this->providers as $provider) {
			$this->app->register($provider);
		}
	}

	/**
	 * Register class aliases.
	 */
	public function registerAliases()
	{
		AliasLoader::getInstance($this->aliases)->register();
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('modulemanager.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'modulemanager'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/modulemanager');

		$sourcePath = __DIR__.'/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath
		]);

		$this->loadViewsFrom([$viewPath, $sourcePath], 'modulemanager');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/modulemanager');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'modulemanager');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'modulemanager');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

	/**
	 * Register menus
	 *
	 * @return void
	 */
	public function registerMenus()
	{
		//if (file_exists($path = app_path('menus.php'))) {
		//	require $path;
		$menu = realpath(__DIR__ . '/../menus.php');

		if ( file_exists( $menu ) )
			include( $menu );
	}

	public function registerPermissions()
	{
		$permission = realpath(__DIR__ . '/../permissions.php');

		if ( file_exists( $permission ) )
			include( $permission );

		/*require __DIR__.'/../permissions.php';
		require __DIR__.'/../composers.php';
		require __DIR__.'/../helpers.php';
		require __DIR__.'/../observers.php';
		require __DIR__.'/../menus.php';
		require __DIR__.'/../routes.php';*/
	}
}
