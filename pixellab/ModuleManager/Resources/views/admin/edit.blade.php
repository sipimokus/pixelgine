@extends($layout)

@section('content-header')
	<h1>
		{{ ucwords($module->getName()) }} Module
		&middot;
		<small>{!! link_to_route('admin.modulemanager.index', trans('modulemanager::modules.back')) !!}</small>
	</h1>
@stop

@section('content')
	{{--
	<div>
		@include('modulemanager::admin.form', array('model' => $module))
	</div>
	--}}

	<div>
		<table class="table">
			<thead>
				<tr>
					<th>{{ trans('modulemanager::modules.table.name') }}</th>
					<th>{{ trans('modulemanager::modules.table.path') }}</th>
					<th>{{ trans('modulemanager::modules.table.alias') }}</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ ucwords($module->getName()) }}</td>
					<td>{{ $module->getPath() }}</td>
					<td>{{ $module->getAlias() }}</td>
				</tr>
			</tbody>
		</table>
			{!! modal_popup(route('admin.modulemanager.destroy', $module->getName()), 'Delete module', trans('modulemanager::modules.modal.delete', ['name' => ucwords($module->getName())]))!!}
{{--
			<a href="{!! route('admin.modulemanager.install', $module->getName()) !!}" class="btn">Install module</a>
			--}}
	</div>
@stop
