<?php

return [
	'title'  => 'Modules',
	'modules'=> 'Modules',
	'module' => 'Module',
	'all' 	 => 'All Modules',
	'create' => 'Add New Module',
	'edit'	 => 'Edit',
	'enable' => 'Enable',
	'disable'=> 'Disable',
	'back'	 => 'Back',
	'table'	 =>
	[
		'id'		=> 'no',
		'alias'		=> 'Alias',
		'description'=>'Description',
		'name'		=> 'Name',
		'status'	=> 'Status',
		'path'		=> 'Path',
		'action'	=> 'Action',
		'tags'		=> 'Tags',
	],
	'modal'	=>
	[
		'enable'	=> 'Do you really enable <strong>:name</strong> module?',
		'disable'	=> 'Do you really disable <strong>:name</strong> module?',
		'delete'	=> 'Do you really delete <strong>:name</strong> module?',
	]
];
